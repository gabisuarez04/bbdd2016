//requerir el documento
var Asignacion = require('../models/asignacion');
var Empleado = require('../models/empleado');
var Proyecto = require('../models/proyecto');
var async = require('async');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//Métodos GET para /api/asignaciones
exports.listAsignaciones = function (req, res, next) {
    Asignacion.find(function (err, asignaciones) {
        if (err) {
            logger.error('Error al intentar recuperar todas las asignaciones');
            logger.error('El error es: ' + err);
            return next(err);
        }

        res.json({message: "Las asignaciones son: ", asignaciones: asignaciones});
    });
};

//Métodos GET para /api/asignaciones/id
exports.getAsignacion = function (req, res, next) {
    Asignacion.findOne({_id: req.params.id}, function (err, asignacion) {
        if (err) {
            logger.error('Error al buscar una  asignacion con id: ' + req.params.id + ' el error es:');
            logger.error(err);
            return next(err);
        }
        if (asignacion != undefined) {
            return res.json({message: "La asignacion es: ", asignacion: asignacion});
        } else {
            return res.json({message: 'No existe la asignacion'});
        }
    });
};


// Métodos POST para /api/applications/idEmpleado&idProyecto&cargo&hs
exports.createAsignacion = function (req, res, next) {
    var asignacion = new Asignacion();
    Empleado.findOne({_id: req.params.idEmpleado}, function (err, empleado) {
	if (err) {
	    logger.error('Error al buscar un empleado con id: ' + req.params.id + ' el error es:');
	    logger.error(err);
	    return next(err);
        } if (empleado != undefined) {
	    Proyecto.findOne({_id: req.params.idProyecto}, function (error, proyecto) {
		if (error) {
		   logger.error('Error al buscar el proyecto con id: ' + req.params.idProyecto + ' el error es:');
	           logger.error(err);
		   return next(err);
		} if (proyecto != undefined) {
			asignacion.empleado = req.params.idEmpleado;
 			asignacion.proyecto = req.params.idProyecto;
			asignacion.cargo = req.params.cargo;
			asignacion.hs = req.params.hs;
   			asignacion.save(function (err) {
				if (err) {
				    logger.error('Error al añadir la asignacion; el error es:');
				    logger.error(err);
				    return next(err);
				}
				logger.info('Asignacion añadida con éxito');
				return res.json({message: 'Asignacion añadida', asignacion: asignacion});
		    	});

		} else {
	    		return res.json({message: 'No existe el proyecto'});
        	}
	    });	
        } else {
	    return res.json({message: 'No existe el empleado'});
        }
    });
    

  
};



//Métodos PUT para /api/asignaciones/id&idEmpleado&idProyecto&cargo&hs
exports.updateAsignacion = function (req, res, next) {

	async.parallel ([
	    function (callback) {
		Asignacion.findOne({_id: req.params.id}, function (err, asignacion) {
			if (err) callback("Error en la asignacion: "+err);
			else {
				if (asignacion == undefined) callback("No existe la asignacion");
				else callback (null,asignacion);
				
			}
		}); 
		 
	    },
	    function(callback) {
		Empleado.findOne({_id: req.params.idEmpleado}, function (err, empleado){
			if (err) callback("Error en el empleado: "+err);
			else {
				if (empleado == undefined) callback("No existe el empleado");
				else callback (null);
			}
		}); 
				
	    },
 	    function(callback) {
		Proyecto.findOne({_id: req.params.idProyecto}, function (err, proyecto) {
			if (err) callback("Error en el proyecto: "+err);
			else {
				if (proyecto == undefined) callback("No existe el proyecto");
				else callback (null);
			}

		});

	    }], function(err, results) {
		   if (err) { 
			logger.error('Error al actualizar la asignacion; ');
			logger.error(err);
			return next(err);
		   }
		   else {
			results[0].empleado = req.params.idEmpleado;
			results[0].proyecto = req.params.idProyecto;
			results[0].cargo = req.params.cargo;
			results[0].hs = req.params.hs;
			results[0].save(function (error) {
			if (error) {
				logger.error('Error al actualizar la asignacion; el error es: ');
			        logger.error(error);
				return next(error);

			} 
			logger.info('Se actualizó la asignacion con id: ' + req.params.id);
				res.json({message: 'Asignacion updated!', asignacion: results[0]});
			});
		   }	

                }
	);


};





//Métodos DELETE de /api/asignaciones/id
exports.deleteAsignacion = function (req, res, next) {
    
    Asignacion.remove({_id: req.params.id}, function (err, asignacion) {
        if (err) {
                logger.error('Error al intentar borrar la asignacion: ' + req.params.id, ', el error es:');
                logger.error(err);
                return next(err);
        }
	
        if (asignacion.result["n"] != 0) {
        	logger.info('La asignacion: ' + req.params.id + ' fue eliminada exitosamente');
        	res.json({message: 'Eliminacion exitosa'});
	} else { res.json({message: 'No existe la asignacion'}); } 
    });
};
