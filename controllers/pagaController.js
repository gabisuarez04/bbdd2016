var Paga = require('../models/paga');
var Empleado = require('../models/empleado');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//Métodos GET para /api/pagas
exports.listPagas = function (req, res, next) {
    Paga.find(function (err, pagas) {
        if (err) {
            logger.error('Error al intentar recuperar todas las pagas');
            logger.error('El error es: ' + err);
            return next(err);
        }
        res.json({message: "Las Pagas son: ", pagas: pagas});
    });
};

//Métodos GET para /api/pagas/id
exports.getPaga = function (req, res, next) {
    Paga.findOne({_id: req.params.id}, function (err, paga) {
        if (err) {
            logger.error('Error al buscar una  paga con id: ' + req.params.id + ' el error es:');
            logger.error(err);
            return next(err);
        }
        if (paga != undefined) {
            return res.json({message: "La paga es: ", paga: paga});
        } else {
            return res.json({message: 'No existe la paga'});
        }
    });
};


// Métodos POST para /api/pagas/id/salario
exports.createPaga = function (req, res, next) {
    var paga = new Paga();
    paga._id = req.params.id;
    paga.salario = req.params.salario;
    paga.save(function (err) {
        if (err) {
            logger.error('Error al añadir Paga:');
            logger.error(err);
            return next(err);
        }
        logger.info('Paga añadida con éxito');
        return res.json({message: 'Paga añadida', paga: paga});
    });

};

//Métodos DELETE de /api/pagas/id
exports.deletePaga = function (req, res, next) {

   Empleado.findOne({titulo: req.params.id}, function (err, empleado) {
	if (err) {
	    logger.error('Error al buscar el empleado con paga: ' + req.params.id + ' el error es:');
	    logger.error(err);
	    return next(err);
	}
	if (empleado != undefined) {
	    return res.json({message: 'La paga se encuentra relacionada a un empleado; No se puede eliminar'});
	} else {
		 Paga.remove({_id: req.params.id}, function (err, paga) {
			    if (err) {
				logger.error('Error al intentar borrar la Paga: ' + req.params.id, ' el error es:');
				logger.error(err);
				return next(err);
			    }
			    logger.info('La Paga con id: ' + req.params.id + ', fue eliminada exitosamente');
			    res.json({message: 'Eliminacion exitosa'});
		 });
	}
    });


};

//Métodos PUT para /api/pagas/id/salario
exports.updatePaga = function (req, res, next) {
       Paga.findOne({_id: req.params.id },
        function (err, paga) {
            if (err) {
                logger.error('Error al actualizar la paga: ' + req.params.id + ', el error es:');
                logger.error(err);
                return next(err);
            }
            //modifico los atributos 
            paga.salario = req.params.salario;
            // guardo 
            paga.save(function (err) {
                if (err) {
                    return res.json(err);
                }
                logger.info('Se actualizó la paga con id: ' + req.params.id);
                res.json({message: 'Paga updated!', paga: paga});
            });
        });
};

