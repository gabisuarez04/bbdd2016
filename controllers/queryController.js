
var Paga = require('../models/paga');
var Empleado = require('../models/empleado');
var Asignacion = require('../models/asignacion');
var Proyecto = require('../models/proyecto');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

// Metodo GET  /api/queries/empleados/
exports.listEmpleados = function (req, res, next) {
   
   	  Empleado
	  .find()
	  .populate('titulo')
	  .exec(function (err, empleados) {

		  if (err) {
		    logger.error('Error al recuperar todos los empleados');
		    logger.error(err);
		    return next(err);
		  }
	          res.json({message: "Los empleados son: ", empleados: empleados});		 
           });
};

// Metodo DELETE /api/queries/empleados/id
/*borra al empleado de la coleccion de empleados y asignaciones*/
exports.deleteEmpleado = function (req, res, next) {
	// Remove todas las asignaciones que referencian al empleado
	Asignacion.remove({empleado: req.params.id}, function (err){
		if (err) {
		    logger.error('Error al eliminar las asignaciones del empleado');
		    logger.error(err);
		    return next(err);
        	}
		  Empleado.remove({_id: req.params.id}, function (err){
		   	  if (err) {
			    logger.error('Error al eliminar el empleado');
			    logger.error(err);
			    return next(err);
			}
	        	  res.json({message: "Las asignaciones del empleado y el empleado se eliminaron de manera exitosa"});		 
        	  });
	});

};

// Metodo GET /api/queries/asignaciones/
exports.listAsignaciones = function (req, res, next) {
/*populate path lo uso para que me cambie la referencia por el correspondiente documento, cuando tengo referencias 
a traves de distintos niveles*/
   	  Asignacion
	  .find()
	  .populate('proyecto')
	  .populate({
		path: 'empleado',
		populate: { path: 'titulo'}
	  })
	  .exec(function (err, asignaciones) {
		  if (err) {
		    logger.error('Error al recuperar todos las asignaciones');
		    logger.error(err);
		    return next(err);
		  }
	          res.json({message: "Las asignaciones son: ", asignaciones: asignaciones});		 
           });
};

// Metodo GET /api/queries/asignaciones/id
exports.listAsignacionesEmpleado = function (req, res, next) {
	Asignacion.
	  find({
	    empleado: req.params.id,
	    hs: { $gt: 10, $lt: 30 },
	  }).
          sort({ proyecto: 1 }).
          select({ proyecto: 1, hs: 1 })
          .exec(function (err, asignaciones) {
		  if (err) {
		    logger.error('Error al recuperar todos las asignaciones');
		    logger.error(err);
		    return next(err);
		  }
	          res.json({message: "Las asignaciones son: ", asignaciones: asignaciones});		 
           });

};

// Metodo DELETE /api/queries/asignaciones/
exports.deleteAllAsignaciones = function (req, res, next) {
/*los {} del remove incluye a todos los documentos de la coleccion*/
	Asignacion.remove({}, function (err) {
		  if (err) {
		    logger.error('Error al recuperar todos las asignaciones');
		    logger.error(err);
		    return next(err);
		  }
	          res.json({message: "Las asignaciones se eliminaron de manera exitosa"});		 
        });

};

