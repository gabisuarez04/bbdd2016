
var Empleado = require('../models/empleado');
var Paga = require('../models/paga');
var Asignacion = require('../models/asignacion');
var async = require('async');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//Métodos GET para /api/empleados o /api/empleados/?join=y
exports.listEmpleados = function (req, res, next) {
   
 if (req.query.join == 'y') {
	  Empleado
	  .find()
	  .populate('titulo')
	  .exec(function (err, empleados) {

		  if (err) {
		    logger.error('Error al recuperar todos los empleados');
		    logger.error(err);
		    return next(err);
		  }
	          res.json({message: "Los empleados son: ", empleados: empleados});		 
	  });

  } else {


	 Empleado.find(function (err, empleados) {
		if (err) {
		    logger.error('Error al intentar recuperar todos los empleados');
		    logger.error('El error es: ' + err);
		    return next(err);
		}
		res.json({message: "Los empleados son: ", empleados: empleados});
	    });

   }

};


//Métodos GET para /api/empleados/id o /api/empleados/id?join=y
exports.getEmpleado = function (req, res, next) {

	if (req.query.join == 'y') {

		Empleado
		.findOne({ _id: req.params.id })
		.populate('titulo')
		.exec(function (err, empleado) {

			  if (err) {
			    logger.error('Error al buscar un empleado con id: ' + req.params.id + ' el error es:');
			    logger.error(err);
			    return next(err);
			  }
			  if (empleado != undefined) {
				    return res.json({message: "El empleado es: ", empleado: empleado});
			  } else {
				    return res.json({message: 'No existe el empleado'});
			  }
			 
		});

	} else {
	
		Empleado.findOne({_id: req.params.id}, function (err, empleado) {
			if (err) {
			    logger.error('Error al buscar un empleado con id: ' + req.params.id + ' el error es:');
			    logger.error(err);
			    return next(err);
			}
			if (empleado != undefined) {
			    return res.json({message: "El empleado es: ", empleado: empleado});
			} else {
			    return res.json({message: 'No existe el empleado'});
			}
		});

	}

};


// Métodos POST para /api/empleados/nombre/titulo
exports.createEmpleado = function (req, res, next) {
    Paga.findOne({_id: req.params.titulo}, function (err, paga) {
	if (err) {
	    logger.error('Error al buscar la paga con id: ' + req.params.titulo + ' el error es:');
	    logger.error(err);
	    return next(err);
	}
	if (paga != undefined) {
	    var empleado = new Empleado();
	    empleado.nombre = req.params.nombre;
	    empleado.titulo = req.params.titulo;
	    empleado.save(function (err) {
		if (err) {
		    logger.error('Error al añadir el empleado:');
		    logger.error(err);
		    return next(err);
		}
		logger.info('Empleado añadido con éxito');
		return res.json({message: 'Empleado añadido', empleado: empleado});
	    });
	} else {
	    return res.json({message: 'No existe la paga especificada'});
	}
    });


};


//Métodos PUT para /api/empleados/id/nombre/titulo
// modifico los atributos del empleado
exports.updateEmpleado = function (req, res, next) {

	async.parallel ([
	    function(callback) {
		Empleado.findOne({_id: req.params.id}, function (err, empleado){
			if (err) callback("Error en la busqueda del empleado: "+err);
			else {
				if (empleado == undefined) callback("No existe el empleado");
				else callback (null,empleado);
			}
		}); 
				
	    },
 	    function(callback) {
		Paga.findOne({_id: req.params.titulo}, function (err, paga) {
			if (err) callback("Error en la busqueda de la paga: "+err);
			else {
				if (paga == undefined) callback("No existe una paga con ese titulo");
				else callback (null);
			}

		});

	    }], function(err, results) {
		   if (err) { 
			logger.error('Error al actualizar el empleado; ');
			logger.error(err);
			return next(err);
		   }
		   else {
			results[0].nombre = req.params.nombre;
			results[0].titulo = req.params.titulo;
			results[0].save(function (error) {
				if (error) {
					logger.error('Error al actualizar el empleado; el error es: ');
			       		logger.error(error);
					return next(error);
				} 
				logger.info('Se actualizó el empleado con id: ' + req.params.id);
				res.json({message: 'Empleado updated!', empleado: results[0]});
			});
		   }	

                }
	);


};


//Métodos DELETE de /api/empleados/id
exports.deleteEmpleado = function (req, res, next) {

    Asignacion.findOne({empleado: req.params.id}, function (err, asignacion) {
	if (err) {
	    logger.error('Error al buscar el empleado con id: ' + req.params.id + ' el error es:');
	    logger.error(err);
	    return next(err);
	}
	if (asignacion != undefined) {
	    return res.json({message: 'El empleado se encuentra asignado a un proyecto; No se puede eliminar'});
	} else {
		Empleado.remove({_id: req.params.id}, function (err, empleado) {
                	if (err) {
                		logger.error('Error al intentar borrar el empleado: ' + req.params.id, ' el error es:');
		                logger.error(err);
                		return next(err);
                	}
                	logger.info('El empleado: ' + req.params.id + ' fue eliminado exitosamente');
                	return res.json({message: 'Eliminacion exitosa'});
                });

	}
    });


};


