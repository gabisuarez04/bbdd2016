
var Proyecto = require('../models/proyecto');
var Asignacion = require('../models/asignacion');

//requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

//Métodos GET para /api/proyectos 
exports.listProyectos = function (req, res, next) {
   
   Proyecto.find(function (err, proyectos) {
		if (err) {
		    logger.error('Error al intentar recuperar todos los proyectos');
		    logger.error('El error es: ' + err);
		    return next(err);
		}
		res.json({message: "Los proyectos son: ", proyectos: proyectos});
    });

};


//Métodos GET para /api/proyectos/id 
exports.getProyecto = function (req, res, next) {

	Proyecto.findOne({_id: req.params.id}, function (err, proyecto) {
		if (err) {
		    logger.error('Error al buscar un proyecto con id: ' + req.params.id + ', el error es:');
		    logger.error(err);
		    return next(err);
		}
		if (proyecto != undefined) {
		    return res.json({message: "El proyecto es: ", proyecto: proyecto});
		} else {
			    return res.json({message: 'No existe el proyecto'});
		}
	});

	

};


// Métodos POST para /api/proyectos/nombre/presupuesto
exports.createProyecto = function (req, res, next) {
    var proyecto = new Proyecto();
    proyecto.nombre = req.params.nombre;
    proyecto.presupuesto = req.params.presupuesto;
    proyecto.save(function (err) {
        if (err) {
            logger.error('Error al añadir el proyecto, el error es:');
            logger.error(err);
            return next(err);
        }
        logger.info('Proyecto añadido con éxito');
        return res.json({message: 'Proyecto añadido', proyecto: proyecto});
    });

};


//Métodos PUT para /api/proyectos/id/nombre/presupuesto
exports.updateProyecto = function (req, res, next) {
    Proyecto.findOne({_id: req.params.id },
        function (err, proyecto) {
            if (err) {
                logger.error('Error al actualizar el proyecto: ' + req.params.id + ', el error es:');
                logger.error(err);
                return next(err);
            }
            //modifico los atributos 
            proyecto.nombre = req.params.nombre;
	    proyecto.presupuesto = req.params.presupuesto;
            // guardo 
            proyecto.save(function (err) {
                if (err) {
                    return res.json(err);
                }
                logger.info('Se actualizó el proyecto con id: ' + req.params.id);
                res.json({message: 'Proyecto updated!', proyecto: proyecto});
            });
    });
};


//Métodos DELETE de /api/proyectos/id
exports.deleteProyecto = function (req, res, next) {

  Asignacion.findOne({proyecto: req.params.id}, function (err, asignacion) {
	if (err) {
	    logger.error('Error al buscar el proyecto con id: ' + req.params.id + ' el error es:');
	    logger.error(err);
	    return next(err);
	}
	if (asignacion != undefined) {
	    return res.json({message: 'El proyecto se encuentra asignado a un empleado; No se puede eliminar'});
	} else {
		Proyecto.remove({_id: req.params.id}, function (err, proyecto) {
		    if (err) {
		        logger.error('Error al intentar borrar el proyecto: ' + req.params.id, ' el error es:');
		        logger.error(err);
		        return next(err);
		    }
		   logger.info('El proyecto: ' + req.params.id + ' fue eliminado exitosamente');
		   res.json({message: 'Eliminacion exitosa'});
		   
		});	

	}
    });


};


