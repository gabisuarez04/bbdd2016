var Paga = require('../models/paga');
var Empleado = require('../models/empleado');
var Asignacion = require('../models/asignacion');
var Proyecto = require('../models/proyecto');

var async = require('async');
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

/* -------------------------------- POST -------------------------------- */

exports.post = function (req, res, next) {
  
	var cantInsertar = req.params.cant;

	// Arreglo de Pagas 

	var pagasArray = new Array();

	for (i = 0; i < cantInsertar; i++) {
	    var documento = {
		_id: "Titulo "+i,	
		salario: 12000
	    };
	    pagasArray[i] = documento;
	};


	// Arreglo de Proyectos   

	var proyectosArray = new Array();

	for (i = 0; i < cantInsertar; i++) {   
	    var documento = {
		_id: i,    
		nombre: "Desarrollo BD",
		presupuesto: 50000
	    };
	    proyectosArray[i] = documento;
	};


	// Arreglo de Empleados 

	var empleadosArray = new Array();

	for (i = 0; i < cantInsertar; i++) {
	    var documento = {
		_id: i,
	    	nombre: "Juan",
		titulo: "Titulo "+1
	    };
	    empleadosArray[i] = documento;
	};

	// Arreglo de Asignaciones 

	var asignacionesArray = new Array();

	for (i = 0; i < cantInsertar; i++) {
	    var documento = {
		_id: i,
		empleado: 1,
	    	proyecto: i,
	    	cargo: "Analista",
	    	hs: 10
	    };
	    asignacionesArray[i] = documento;
	};

	// ------ INSERTA ------ //

	var fin;
	var principio = Date.now();

	async.parallel ([
		    function (callback) {

			Asignacion.insertMany(asignacionesArray,function(err){
				asignacionesArray=[];
				if (err) callback('Error al añadir las asignaciones:'+err);
				else callback(null); 
			
			});

		     },
		    function (callback) {

			Empleado.insertMany(empleadosArray,function(err){
				empleadosArray=[];
				if (err) callback('Error al añadir los empleados:'+err);
				else callback(null);
			});

		     },
		    function (callback) {

			Paga.insertMany(pagasArray,function(err){
				pagasArray=[];
				if (err) callback("Error al añadir Pagas: "+err);
				else callback(null);
			
			});
	  		
		     },
		     function(callback) {
			Proyecto.insertMany(proyectosArray,function(err){
				proyectosArray=[];
				if (err) callback("Error al añadir los proyectos: "+err);
				else callback(null);
			});


		    }], function(err, results) {
			   if (err) { 
				logger.error(err);
				return next(err);
			   }
			   else {
	
				fin = Date.now();
				logger.info('Se añadieron Pagas, Proyectos, Empleados y Asignaciones correctamente');				
				logger.info('Cantidad de documentos insertados: '+ cantInsertar*4);
				logger.info('Tiempo de ejecucion: '+ (fin - principio) / 1000.0 + 's');				        
				return res.json({message: 'Documentos añadidos'});
			   }
					
		      }
		);
};

/* -------------------------------- GET -------------------------------- */

exports.get = function (req, res, next) {

	var fin;
	var principio = Date.now();

	Asignacion
		  .find()
		  .populate('proyecto')
		  .populate({
			path: 'empleado',
			populate: { path: 'titulo'}
		  })
		  .exec(function (err, asignaciones) {
			  if (err) {
				  logger.error('Error al recuperar todos las asignaciones');
				  logger.error(err);
				  return next(err);
			  } else {
 				 fin = Date.now();
				 // console.log(JSON.stringify(asignaciones,null,2)); // stringify convierte al JSON en string; el 2 es para que lo imprima 'pretty'
				 logger.info('Se han recuperado las asignaciones de manera exitosa');
				 logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');
				 return res.json({message: "Las asignaciones han sido recuperadas con exito"});
			  }
		   });

};

/* -------------------------------- PUT -------------------------------- */

exports.update = function (req, res, next) {

	var fin;
	var principio = Date.now();

	Asignacion.update({empleado: 1},{$set: { cargo: "programador", hs: 15 }},{ multi: true }, function (err){
		if (err) { 
			logger.error('Error al modificar las asignaciones:'+err); 
			return next(err);
		} else { 	
			fin = Date.now();
			logger.info('Se actualizaron las asignaciones correctamente'); 
			logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');
			return res.json({message: "Se actualizaron las Asignaciones correctamente"});
		
		}
			
	});

};

/* -------------------------------- DELETE -------------------------------- */

exports.deleteAll = function (req, res, next) {

	var fin;
	var principio = Date.now();

	async.parallel ([
		    function (callback) {

			Asignacion.remove({},function(err){
			if (err) callback('Error al eliminar las asignaciones:'+err);
			else callback(null); 
			});

		     },
		    function (callback) {

			Empleado.remove({},function(err){
			if (err) callback('Error al eliminar los empleados:'+err);
			else callback(null);
			});

		     },
		    function (callback) {

			Paga.remove({},function(err){
			if (err) callback("Error al eliminar Pagas: "+err);
			else callback(null);
			
			});
			  		
		     },
		     function(callback) {
			Proyecto.remove({},function(err){
			if (err) callback("Error al eliminar los proyectos: "+err);
			else callback(null);
			});


		    }], function(err, results) {
			   if (err) { 
				logger.error(err);
				return next(err);
			   }
			   else {

				fin = Date.now();
				logger.info('Se eliminaron todas las Pagas, Proyectos, Empleados y Asignaciones correctamente');				
				logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');
				return res.json({message: "Se eliminaron todas las Pagas, Proyectos, Empleados y Asignaciones correctamente"});		
			   }
					
		     }
	);

};


