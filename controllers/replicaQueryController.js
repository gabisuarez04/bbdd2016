var Paga = require('../models/paga');
var Empleado = require('../models/empleado');
var Asignacion = require('../models/asignacion');
var Proyecto = require('../models/proyecto');

// requerir el loger
var myLogClass = require('../utils/logger');
var logger = new myLogClass();

// crea 50 proyectos
exports.createProyecto = function (req, res, next) {
    var proyecto = new Proyecto();
    proyecto.nombre = "uno";
    proyecto.presupuesto = 50000;
    var array = [];
    for (i = 0; i < 10000; i++) {
    	array.push(proyecto);
    }	
    Proyecto.create(array, function (err) {
        if (err) {
            logger.error('Error al añadir los proyectos, el error es:');
            logger.error(err);
            return next(err);
        }
        logger.info('Proyectos añadidos con éxito');
        return res.json({message: 'Proyectos añadidos'});
    });

};

