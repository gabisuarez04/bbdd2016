var asignacionController = require('../controllers/asignacionController');
var express = require('express');
var router = express.Router();

/* /api/asignaciones */

router.route('/')
    .get(asignacionController.listAsignaciones);

router.route('/:id')
    .get(asignacionController.getAsignacion)
    .delete(asignacionController.deleteAsignacion);
   
router.route('/:idEmpleado&:idProyecto&:cargo&:hs')
    .post(asignacionController.createAsignacion);

router.route('/:id&:idEmpleado&:idProyecto&:cargo&:hs')
    .put(asignacionController.updateAsignacion);

module.exports = router;
