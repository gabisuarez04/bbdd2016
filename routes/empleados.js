var empleadoController = require('../controllers/empleadoController');
var express = require('express');
var router = express.Router();

/*  /api/empleados  */
router.route('/')
    .get(empleadoController.listEmpleados)
    
router.route('/:id')
    .get(empleadoController.getEmpleado)
    .delete(empleadoController.deleteEmpleado);

router.route('/:nombre/:titulo')
    .post(empleadoController.createEmpleado);

router.route('/:id/:nombre/:titulo')
    .put(empleadoController.updateEmpleado);

module.exports = router;
