var queryController = require('../controllers/queryController');
var replicaController = require('../controllers/replicaQueryController');
var finalController = require('../controllers/finalController');
var express = require('express');
var router = express.Router();

/*        /api/queries           */
router.route('/empleados/')
    .get(queryController.listEmpleados);

router.route('/empleados/:id')
    .delete(queryController.deleteEmpleado);
    
router.route('/asignaciones/')
    .get(queryController.listAsignaciones)
    .delete(queryController.deleteAllAsignaciones);

router.route('/asignaciones/:id')
    .get(queryController.listAsignacionesEmpleado);

router.route('/replica/proyectos')
    .get(replicaController.createProyecto);

// --------- QUERIES FINAL ----------- //

router.route('/post/:cant')
    .post(finalController.post);

router.route('/get/')
    .get(finalController.get);

router.route('/delete/')
    .delete(finalController.deleteAll);

router.route('/put/')
    .put(finalController.update);

// ----------------------------------- //

module.exports = router;
