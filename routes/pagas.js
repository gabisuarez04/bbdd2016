var pagaController = require('../controllers/pagaController');
var express = require('express');
var router = express.Router();

/*      /api/pagas      */
router.route('/')
    .get(pagaController.listPagas);

router.route('/:id')
    .get(pagaController.getPaga)
    .delete(pagaController.deletePaga);

router.route('/:id/:salario')
    .post(pagaController.createPaga)
    .put(pagaController.updatePaga);

module.exports = router;
