var proyectoController = require('../controllers/proyectoController');
var express = require('express');
var router = express.Router();

/*      /api/proyectos      */
router.route('/')
    .get(proyectoController.listProyectos);
   
router.route('/:id')
    .get(proyectoController.getProyecto)
    .delete(proyectoController.deleteProyecto);

router.route('/:nombre/:presupuesto')
    .post(proyectoController.createProyecto);

router.route('/:id/:nombre/:presupuesto')
    .put(proyectoController.updateProyecto);

module.exports = router;
