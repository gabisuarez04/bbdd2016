//Load require packages
var express = require('express');
var mongoose = require('mongoose');
//auto increment los uso para utilizar _id auto incremental en los equemas 
var autoIncrement = require('mongoose-auto-increment');
var config = require('../config');
var bodyParser = require('body-parser');
var passport = require('passport');

var app = express(); //Create the Express app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(passport.initialize());

//routes are defined here
var apps = require('../routes/applications');

// ----------- CONFIGURACION REPLICA ------------------ //

var urlMongoose = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + ",163.10.33.202:27019,163.10.33.207:27021" + "/" + config.Mongo.dbName;

var options = { 
      db:{
//	readPreference:"secondary",
	readConcern: {level: "local"},
	w: 1,
	wtimeout: 0,
	j: false, 
      },
	server: {
            socketOptions: {
           	 keepAlive: 1,
         	  // poolSize: 10,
          	  connectTimeoutMS: 800000,
          	  socketTimeoutMS: 800000,
            }
      },
      replset:{ 
	connectWithNoPrimary: true, // sirve para que se conecte a la replica por mas de que no encuentre inicialmente al primario
	slaveOk: true,
        socketOptions: {
		keepAlive: 1,
		connectTimeoutMS: 800000, // timeout de la conexion, por defecto es 0 y si la conexion tardaba me daba error
		socketTimeoutMS: 800000
	} 
     } 
};

var connection = mongoose.connect(urlMongoose,options,function (err) {
    if (err) throw err;
    else { console.log("CONEXION CON REPLICA ESTABLECIDA CON EXITO");};	
}); //en la funcion connect() pasas el path de la bd como argumento

//mongoose.set("debug", true);


// ----------- FIN CONFIGURACION REPLICA -------------- //


/*

var urlMongoose = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + "/" + config.Mongo.dbName + "?readPreference=secondary";

var connection = mongoose.connect(urlMongoose, function (err) {
    if (err) throw err;
}); //en la funcion connect() pasas el path de la bd como argumento
*/

autoIncrement.initialize(connection);

var port = process.env.PORT || 8000;

// Create our Express router
var router = express.Router();

// Initial dummy route for testing
router.get('/', function (req, res) {
    res.json({message: 'App Working!'});
});

// Register all our routes with /api
app.use('/api', router);
app.use('/api/applications', apps);


//error handler para todas las rutas
app.use(function (err, req, res, next) {
    if (err) {
        console.log(err);
        res.status(500).send(err);
    }
});


//routes are defined here
var pagas = require('../routes/pagas');
var empleados = require('../routes/empleados');
var proyectos = require('../routes/proyectos');
var asignaciones = require('../routes/asignaciones');
var queries = require('../routes/queries');

// Register all our routes with /api

app.use('/api/pagas', pagas);
app.use('/api/empleados', empleados);
app.use('/api/proyectos', proyectos);
app.use('/api/asignaciones', asignaciones);
app.use('/api/queries', queries);


// Start the server
app.listen(port);
exports.app = app;

console.log('Express is running in port: ' + port);

// ------------------------------ SCRIPT NODE FINAL ------------------------------------

/* En esta seccion se desarrollan las operaciones GET, POST, PUT y DELETE
a nivel Mongoose. Para probar cada una de ellas, se deberá correr el server 
de la siguiente manera:	  

GET: nodemon ./bin/server.js get

POST: nodemon ./bin/server.js post 'cantidad de documentos a insertar'

PUT: nodemon ./bin/server.js put

DELETE: 

*/

var Asignacion = require('../models/asignacion');
var Empleado = require('../models/empleado');
var Proyecto = require('../models/proyecto');
var Paga = require('../models/paga');
var async = require('async');

var myLogClass = require('../utils/logger');
var logger = new myLogClass();

switch (process.argv[2]) {

	// --------------------------- POST ------------------------------
	case 'post':
	
			var cantInsertar = process.argv[3];
			var fin;

			// Arreglo de Pagas 

			var pagasArray = new Array();

			for (i = 0; i < cantInsertar; i++) {
			    var documento = {
				_id: "Titulo "+i,	
				salario: 12000
			    };
			    pagasArray[i] = documento;
			};


			// Arreglo de Proyectos   

			var proyectosArray = new Array();

			for (i = 0; i < cantInsertar; i++) {   
			    var documento = {
				_id: i,    
				nombre: "Desarrollo BD",
				presupuesto: 50000
			    };
			    proyectosArray[i] = documento;
			};


			// Arreglo de Empleados 

			var empleadosArray = new Array();

			for (i = 0; i < cantInsertar; i++) {
			    var documento = {
				_id: i,
			    	nombre: "Juan",
				titulo: "Titulo "+1
			    };
			    empleadosArray[i] = documento;
			};

			// Arreglo de Asignaciones 

			var asignacionesArray = new Array();

			for (i = 0; i < cantInsertar; i++) {
			    var documento = {
				_id: i,
				empleado: 1,
			    	proyecto: i,
			    	cargo: "Analista",
			    	hs: 10
			    };
			    asignacionesArray[i] = documento;
			};

			// ------ INSERTA ------ //

			var principio = Date.now();

			async.parallel ([
				    function (callback) {

					Asignacion.insertMany(asignacionesArray,function(err){
						asignacionesArray=[];
						if (err) callback('Error al añadir las asignaciones:'+err);
						else callback(null); 
			
					});

				     },
				    function (callback) {

					Empleado.insertMany(empleadosArray,function(err){
						empleadosArray=[];
						if (err) callback('Error al añadir los empleados:'+err);
						else callback(null);
					});

				     },
				    function (callback) {

					Paga.insertMany(pagasArray,function(err){
						pagasArray=[];
						if (err) callback("Error al añadir Pagas: "+err);
						else callback(null);
			
					});
			  		
				     },
				     function(callback) {
					Proyecto.insertMany(proyectosArray,function(err){
						proyectosArray=[];
						if (err) callback("Error al añadir los proyectos: "+err);
						else callback(null);
					});


				    }], function(err, results) {
					   if (err) { 
						logger.error(err);
					   }
					   else {
						fin = Date.now();
						logger.info('Se añadieron Pagas, Proyectos, Empleados y Asignaciones correctamente');				
						logger.info('Cantidad de documentos insertados: '+ cantInsertar*4);
						logger.info('Tiempo de ejecucion: '+ (fin - principio) / 1000.0 + 's');			
					   }
					
				      }
				);




	break;

	// --------------------------- GET ------------------------------
	case 'get':

			  var fin;
			  var principio = Date.now();
		 	  
			  Asignacion
			  .find()
			  .populate('proyecto')
			  .populate({
				path: 'empleado',
				populate: { path: 'titulo'}
			  })
			  .exec(function (err, asignaciones) {
				  if (err) {
					  logger.error('Error al recuperar todos las asignaciones');
					  logger.error(err);
				  } else {
					  
					  fin = Date.now();
			//		  console.log(JSON.stringify(asignaciones,null,2)); // stringify convierte al JSON en string; el 2 es para que lo imprima 'pretty'
					  logger.info('Se han recuperado las asignaciones de manera exitosa');
					  logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');
				  }
			   });

	break;
	
	// --------------------------- DELETE ------------------------------

	case 'delete':

			var fin;
			var principio = Date.now();

 			async.parallel ([
				    function (callback) {

					Asignacion.remove({},function(err){
						if (err) callback('Error al eliminar las asignaciones:'+err);
						else callback(null); 
			
					});

				     },
				    function (callback) {

					Empleado.remove({},function(err){
						if (err) callback('Error al eliminar los empleados:'+err);
						else callback(null);
					});

				     },
				    function (callback) {

					Paga.remove({},function(err){
						if (err) callback("Error al eliminar Pagas: "+err);
						else callback(null);
			
					});
			  		
				     },
				     function(callback) {
					Proyecto.remove({},function(err){
						if (err) callback("Error al eliminar los proyectos: "+err);
						else callback(null);
					});


				    }], function(err, results) {
					   if (err) { 
						logger.error(err);
					   }
					   else {
						fin = Date.now();
						logger.info('Se eliminaron todas las Pagas, Proyectos, Empleados y Asignaciones correctamente');				
						logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');		
					   }
					
				      }
				);

			

	break;

	// --------------------------- PUT ------------------------------		

	case 'put':

		var fin;
		var principio = Date.now();
			
		Asignacion.update({empleado: 1},{$set: { cargo: "programador", hs: 15 }},{ multi: true }, function (err){
			if (err) { 
				logger.error('Error al actualizar las asignaciones:'+err);

			} else { 
				fin = Date.now();
				logger.info('Se actualizaron las asignaciones correctamente'); 
				logger.info('Tiempo de ejecucion: ' + (fin - principio) / 1000.0 + 's');
			}			
		});

	break;

}




