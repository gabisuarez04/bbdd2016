var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Paga = new Schema({
    _id: String,	
    salario: Number
});

module.exports = mongoose.model('Paga', Paga);
// model --> define el nombre de la coleccion
// exports --> exporta el modelo para que pueda ser usado en los demas archivos

