var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema = mongoose.Schema;

var Asignacion = new Schema({
    empleado: {type: Number, ref: 'Empleado'},
    proyecto: {type: Number, ref: 'Proyecto'},
    cargo: String,
    hs: Number
});

Asignacion.index({ empleado: 1 , proyecto: 1 },{unique: true});
Asignacion.plugin(autoIncrement.plugin, 'Asignacion');
module.exports = mongoose.model('Asignacion', Asignacion);
