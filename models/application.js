// import the necessary modules
//@see http://mongoosejs.com/docs/guide.html
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// define schema
var ApplicationSchema = new Schema({
    app_key: String,
    app_secret: String,
    users: [{type: Schema.ObjectId, ref: "User"}],
    created_at: Date,
    updated_at: Date
});

module.exports = mongoose.model('Application', ApplicationSchema);
