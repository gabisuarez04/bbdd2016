var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema = mongoose.Schema;

var Proyecto = new Schema({
    nombre: String,	
    presupuesto: Number
});

Proyecto.plugin(autoIncrement.plugin, 'Proyecto');
module.exports = mongoose.model('Proyecto', Proyecto);
