var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema = mongoose.Schema;

var Empleado = new Schema({
    nombre: String,
    titulo: {type: String, ref: 'Paga'}
});

Empleado.plugin(autoIncrement.plugin, 'Empleado');
module.exports = mongoose.model('Empleado', Empleado);
